#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2023 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
"""
Utilities for finding out where glyphs should be placed.
"""

import sys
from inkex import elements

def to_list(node, attr, default="0"):
    """Turn a list of positions, rotations, or other kerning into dimentionless units"""
    return [ node.to_dimensionless(val)
        for val in node.get(attr, default).replace(",", " ").split() if val]

def find_text_root(node):
    """
    Finds the root text object for this text node.
    """
    if isinstance(node, elements.TextElement):
        return node
    if node.getparent() is not None:
        return find_text_root(node.getparent())
    return None

def get_text(node):
    """Get the full text for the given node, including tspan children"""
    return (node.text or "") + "".join(get_text(tspan) for tspan in node) + (node.tail or "")

def layout_text(node, text, get_width, hint=None):
    """
    A basic text layout engine, very much incomplete, but will defer back
    to inkscape's manual kerning positions if available so use the manual kerns
    annotation action to guarentee the location of chars.
    """
    xs = to_list(node, "x", "")
    ys = to_list(node, "y", "")
    rs = to_list(node, "rs", "")
    dxs = to_list(node, "dx", "")
    dys = to_list(node, "dy", "")

    root = find_text_root(node)
    if root is None:
        sys.stderr.write("Can't find root text node '{node.get_id()}', refusing to render text.\n")
        return

    if root != node:
        # Tspans are treated like parts of the root text with hints
        yield from layout_text(root, text, get_width, (xs, ys))
        return

    full_text = get_text(node)
    style = node.specified_style()

    # If any of these properties are set, we're forcing the use of xs
    text_direction = style.get("direction", "ltr") == "rtl"
    text_orientation = style.get("text-orientation", "auto") in ("sideways", "upright")
    writing_mode = style.get("writing-mode", "lr-tb")  in ("tb-rl", "vertical-lr")

    x_offset = 0
    anchor = node.specified_style().get("text-anchor", "start")
    if anchor in ("middle", "end"):
        sys.stderr.write(f" ! Adjusting text anchors from {anchor} is known to be wrong;"\
                          "please change text to use text-anchor: start;\n")
        dwidth = get_width(text)
        x_offset = dwidth / (int(anchor[0] == "m") + 1)

    pos = full_text.find(text)
    if pos == -1:
        sys.stderr.write("Bad Text layout processing: Can't find '{text}' in '{full_text}'\n")
        return

    # Add tspan x/y to an root xs/ys that doesn't have enough values
    if hint and hint[0] and hint[1] and len(xs) < pos and len(ys) < pos:
        # We append the tspan hints to our master list
        xs = (xs + ([0] * pos))[:pos-1] + hint[0]
        ys = (ys + ([0] * pos))[:pos-1] + hint[1]

    if not xs:
        xs.append(0)
    if not ys:
        ys.append(0)

    def fallback_calc(arr, ds, i, j):
        """Fallback layout calculation where xs wasn't available"""
        return arr[i] + get_width(full_text[i:j]) + sum(ds[i:j])

    # TODO: In the future we could pass dx into the pdf Tj command, but not today
    if rs or len(xs) > 1 or len(ys) > 1 or text_direction or text_orientation or writing_mode:
        # Each glyph is added one by one, this is terribly ineffecient in the PDF though
        for i, c in enumerate(text):
            p = pos + i # Advance to the next location
            r = rs[p] if len(rs) > p else 0
            x = xs[p] if len(xs) > p else fallback_calc(xs, dxs, len(xs)-1, p)
            y = ys[p] if len(ys) > p else fallback_calc(ys, dys, len(ys)-1, p)
            yield ((x - x_offset, y), r, c)
    elif len(xs) > pos:
        # This is going to forgo the xs/ys coordinates and instead we write out as a block
        # There may be future tests which fail here, and we need to make sure we understand
        # this is an optimisation intended to place large blocks of text in ltr-tb without
        # any manual kerning. ANY special treatment will fail here.
        yield ((xs[pos] - x_offset, ys[pos]), 0, text)

