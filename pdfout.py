#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2023 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
"""
Export the SVG document as a PDF file.
"""
import os, sys
import inkex

from inkex.transforms import Transform, BoundingBox
from inkex import AbortExtension, elements, paths, colors

# The recorder is for generating test files for capypdf
if os.environ.get('RECORD', False):
    import recorder as capypdf
else:
    import capypdf

from images import HrefData, ImageData
from styles import get_font_filename
from layout_engine import layout_text, to_list

PT = 1.33333 # Conversion from SVG-px of 96dpi to PDF-pt of 72dpi

minmax = lambda a, b=0.0, c=1.0: min(c, max(b, float(a)))

def log(msg):
    sys.stderr.write(f"{msg}\n")

def get_enum(style, name, enum):
    return dict(enum).get(style.get(name, None), enum[0][1])

class PdfShape:
    """Each SVG element is overlayed with a new class which provides a render_to_pdf function"""
    def render_to_pdf(self, gen, ctx, page=None, for_clip=False):
        with PathPainting(gen, ctx, self.specified_style(), self.transform, for_clip or self.clip) as paint:
            self.render_shape(paint)
        if for_clip is True:
            ctx.cmd_n()

    def on_page(self, page):
        with page.contains(self) as checker:
            if checker.value is None:
                checker.value = page.touches(self.bounding_box())
            return checker.value

    def render_shape(self, ctx):
        """Render the specific shape, default to path"""
        for cmd in self.path.to_absolute().proxy_iterator():
            if cmd.letter in "HV":
                # XXX This should be in CommandProxy in inkex.paths
                cmd = cmd.to_curve().to_line(cmd.previous_end_point)
            elif cmd.letter in "SQT":
                cmd = cmd.to_curve()

            if cmd.letter == "A":
                # Arcs are made up of three curves (approximated)
                for bezier in cmd.to_curves():
                    ctx.cmd_c(*bezier.args)
            elif cmd.letter == "M":
                ctx.cmd_m(*cmd.args)
            elif cmd.letter == "L":
                ctx.cmd_l(*cmd.args)
            elif cmd.letter == "C":
                ctx.cmd_c(*cmd.args)
            elif cmd.letter == "Z":
                ctx.cmd_h()
            else:
                raise ValueError(f"Unknown path command '{cmd.letter}'")

class PdfText:
    """Text elements always attempt to write out as text, not as shapes"""
    def get_xy(self, xs, ys):
        xs.extend(to_list(self, "x", ""))
        ys.extend(to_list(self, "y", ""))
        for tspan in self:
            if isinstance(tspan, PdfText):
                tspan.get_xy(xs, ys)
        return (xs, ys)

    def render_to_pdf(self, gen, ctx, page=None, for_clip=False):
        font = self.root.get_font(gen, self.specified_style())

        with TransformState(gen, ctx, self.transform, for_clip or self.clip):
            # Main text body
            self.render_text(gen, ctx, font, self.text, for_clip)

            # Each tspan child
            for tspan in self:
                if isinstance(tspan, PdfText):
                    tspan.render_to_pdf(gen, ctx, page, for_clip)

                # Each text block sandwiched inbetween or at the end of the text
                self.render_text(gen, ctx, font, tspan.tail, for_clip)

    def render_text(self, gen, ctx, font, text, for_clip=False):
        if not font or not text:
            return

        # Flip text rendering back to normal direction (painting canvase direction)
        ctx.cmd_cm(*self.root.flip.to_hexad())

        style = self.specified_style()
        with TextPainting(gen, ctx, style, None, for_clip) as text_ctx:

            Tf = self.to_dimensionless(style.get("font-size", 10))
            if Tf:
                text_ctx.cmd_Tf(font, Tf)
            else: # Invisible text (0 size)
                return

            Tc = self.to_dimensionless(style.get("letter-spacing", 0))
            if Tc:
                text_ctx.cmd_Tc(Tc)

            Tw = self.to_dimensionless(style.get("word-spacing", 0))
            if Tw:
                text_ctx.cmd_Tw(Tw)

            def get_text_width(_text):
                return gen.text_width(_text, font, Tf)

            for (x, y), r, subset in layout_text(self, text, get_text_width):
                # Flop text baseline point back to responsition it in svg coordinate space
                p = self.root.flip.apply_to_point((x, y))
                tm = Transform(translate=(p.x, p.y))
                # Glyph rotation, should be one glyph if rotated
                if r:
                    tm *= Transform(rotate=r)
                text_ctx.cmd_Tm(*tm.to_hexad())
                text_ctx.render_text(subset)

        # Clipping text doesn't reset the local transform, so we re-flip for the objects that follow.
        if for_clip is True:
            ctx.cmd_cm(*self.root.flip.to_hexad())


class PdfGroup:
    """An element that contains other elements for rendering"""
    def on_page(self, page):
        with page.contains(self) as checker:
            if checker.value is None:
                checker.value = False
                for child in self:
                    if hasattr(child, 'on_page') and child.on_page(page):
                        checker.value = True
            return checker.value

    def render_to_pdf(self, gen, ctx, page=None):
        with TransformState(gen, ctx, transform=self.transform, clip=self.clip):
            for child in self:
                if not hasattr(child, 'render_to_pdf'):
                    # Known meta elements that are never rendered
                    if not isinstance(child, (elements.NamedView, elements.Metadata, elements.Defs, Page)):
                        sys.stderr.write(f"skip:{child.tag_name} (p:{page})\n")
                    continue
                if page is None or child.on_page(page):
                    child.render_to_pdf(gen, ctx, page)


class SvgDocumentElement(PdfGroup, elements.SvgDocumentElement):
    # NOTE: We should be keeping tract of the transforms so we can detect
    # when an object is visible on a given page or not.
    _fontcache = {}
    clip = None
    device_cmyk = None

    def to_pdf(self, output):
        # Scale to SVG's USER_UNIT to PDF pt
        page_properties = capypdf.PageProperties()
        page_properties.set_pagebox(capypdf.PageBox.Media,
            0, 0, self.viewport_width / PT, self.viewport_height / PT)

        options = capypdf.Options()
        # Apply CMYK ICC profile if it's available in the document
        for child in self.defs:
            if not isinstance(child, ColorProfile):
                continue
            # XXX THIS ASSUMES A LOT, it might not be a CMYK profile
            self.device_cmyk = child
            options.set_colorspace(capypdf.Colorspace.DeviceCMYK)
            options.set_device_profile(capypdf.Colorspace.DeviceCMYK, child.get_filename())
            options.set_output_intent(capypdf.IntentSubtype.PDFX, child.name)

        options.set_default_page_properties(page_properties)
        options.set_title(self.metadata.title or "Inkscape Document")
        options.set_author(self.metadata.creator or "")
        options.set_creator(f"Inkscape {inkex.__version__}")

        scale = self.root.scale / PT
        with capypdf.Generator(output, options) as gen:
            # Multi-page document, might have margins
            for page in self.namedview._get_pages():
                with gen.page_draw_context() as ctx:
                    # Set the page size, bleed etc from the page element
                    page_properties = capypdf.PageProperties()
                    (x, y) = page.set_page_size(page_properties, scale)
                    ctx.set_custom_page_properties(page_properties)

                    # Render the page object
                    self._scale_page(ctx, scale, page.height, page.x - x, page.y + y)
                    self.render_to_pdf(gen, ctx, page)

            if not self.namedview._get_pages():
                # This uses the default page properties (no pages)
                with gen.page_draw_context() as ctx:
                    self._scale_page(ctx, scale, self.viewport_height / scale / PT)
                    self.render_to_pdf(gen, ctx)

    def _scale_page(self, ctx, scale, height, x=0, y=0):
        """Put the pdf context into the right svg coordinate space"""
        # 1. Scale from pdf pt (72 dpi) to svg user units
        ctx.scale(scale, scale)
        # 2. Flip y-axis from pdf SW to svg NW
        self.root.flip = Transform([1.0, 0.0, 0.0, -1.0, 0.0, height])
        ctx.cmd_cm(*self.root.flip.to_hexad())
        # 3. Move to the page translation in the svg
        ctx.translate(-x, -y)

    def get_font(self, gen, style):
        """Return the capypdf font object from the given style"""
        ttf = get_font_filename(style)
        if ttf not in self._fontcache and ttf:
            try:
                self._fontcache[ttf] = gen.load_font(ttf)
            except capypdf.CapyPDFException as err:
                self._fontcache[ttf] = None
                sys.stderr.write(f"Failed loading font {font_desc} '{ttf}': {err}\n")
        return self._fontcache.get(ttf, None)


class Page(elements.Page):
    def touches(self, bbox):
        """Returns true if this pages contains/touches any part of the given bounding box"""
        return self._bbox.left < bbox.right and self._bbox.right > bbox.left and \
               self._bbox.top < bbox.bottom and self._bbox.bottom > bbox.top

    def contains(self, elem):
        class Contains:
            """
            A nullable boolean check for if an object is known to be valid for a given page.

            This can come from Inkscape as an annotation, or from a python check on the shape's
            boudning box if no value was provided by inkscape
            """
            attr = "inkscape:on_pages"

            def __init__(self, page, elem):
                self.elem = elem
                self.value = None
                self.page_id = page.get_id()
                self.on_pages = [pid.strip() for pid in elem.get(self.attr, "").split(",") if pid.strip()]

            def __enter__(self):
                if self.page_id in self.on_pages:
                    self.value = True
                elif f"!{self.page_id}" in self.on_pages:
                    self.value = False
                self.was = self.value
                return self

            def __exit__(self, exc_type, exc_value, exc_tb):
                if self.was != self.value:
                    if self.value is True:
                        self.on_pages.append(self.page_id)
                    elif self.value is False:
                        self.on_pages.append(f"!{self.page_id}")
                self.elem.set(self.attr, ",".join(self.on_pages))
        return Contains(self, elem)

    def set_page_size(self, props, scale):
        """Set the page size, bleed, margin etc, returns the required translation"""
        self._bbox = self.bounding_box

        border = 1 # Maybe set this somewhere?
        width = self.width * scale
        height = self.height * scale

        if self.get("bleed") or self.get("margin"):
            margin = self.margin_box() * scale
            bleed = self.bleed_box() * scale
            props.set_pagebox(capypdf.PageBox.Media, 0, 0, bleed.width, bleed.height)
            props.set_pagebox(capypdf.PageBox.Bleed, 0, 0, bleed.width, bleed.height)
            props.set_pagebox(capypdf.PageBox.Trim,
                0 - bleed.minimum.x,
                0 - bleed.minimum.y,
                width - bleed.minimum.x,
                height - bleed.minimum.y
            )
            props.set_pagebox(capypdf.PageBox.Art,
                margin.minimum.x - bleed.minimum.x,
                margin.minimum.y - bleed.minimum.y,
                margin.maximum.x - bleed.minimum.x,
                margin.maximum.y - bleed.minimum.y,
            )
            return (-bleed.minimum.x / scale,
                    (bleed.maximum.y - height) / scale)

        props.set_pagebox(capypdf.PageBox.Media, 0, 0, width, height)
        return (0, 0)

    def _delta_box(self, name, box=None, expand=False) -> BoundingBox:
        """Construct a bounding box from the page box"""
        parse = [self.to_dimensionless(val) * (-1, 1)[expand]
            for val in self.get(name).replace(",", " ").split() if val] or [0]

        while len(parse) < 4:
            parse.append(parse[(0, len(parse) - 2)[len(parse) - 2 >= 0]])

        return BoundingBox(
            (box.x.minimum - parse[3], box.x.maximum + parse[1]),
            (box.y.minimum - parse[0], box.y.maximum + parse[2]),
        )

    def bleed_box(self) -> BoundingBox:
        """Returns the bleed box of the page"""
        page = BoundingBox.new_xywh(0, 0, self.width, self.height)
        return self._delta_box("bleed", page, True)

    def margin_box(self) -> BoundingBox:
        """Returns the margin box of the page"""
        page = BoundingBox.new_xywh(0, 0, self.width, self.height)
        return self._delta_box("margin", page, False)

class Group(PdfGroup, elements.Group):
    pass

class Use(PdfGroup, elements.Use):
    # NOTE: This could use a PDF XObject form to conserve space
    pass

class ClipPath(elements.ClipPath):
    def render_to_clip(self, gen, ctx):
        for elem in self:
            if hasattr(elem, 'render_to_pdf'):
                elem.render_to_pdf(gen, ctx, None, for_clip=True)
            else:
                sys.stderr.write(f"Can't clip with object {self.clip}\n")

class Polyline(PdfShape, elements.Polygon):
    pass

class Polygon(PdfShape, elements.Polygon):
    pass

class Line(PdfShape, elements.Line):
    pass

class Rectangle(PdfShape, elements.Rectangle):
    pass

class Circle(PdfShape, elements.Circle):
    pass

class Ellipse(PdfShape, elements.Ellipse):
    pass

class PathElement(PdfShape, elements.PathElement):
    pass

class Anchor(PdfShape, elements.Anchor):
    pass

class Marker(PdfShape, elements.Marker):
    def render_to_pdf(self, gen, ctx):
        raise ValueError("Refusing the render markers, please remove marker content")

class Symbol(PdfShape, elements.Symbol):
    pass

class Image(PdfShape, ImageData, elements.Image):
    interpolations = (
        ('auto', capypdf.ImageInterpolation.Smooth),
        ('pixelated', capypdf.ImageInterpolation.Pixelated),
        ('crisp-edges', capypdf.ImageInterpolation.Pixelated),
        ('optimizeSpeed', capypdf.ImageInterpolation.Pixelated),
        ('optimizeQuality', capypdf.ImageInterpolation.Smooth),
    )

    def render_to_pdf(self, gen, ctx, page=None, for_clip=False):
        if for_clip:
            raise ValueError("Raster images can not be used as a clipping shape")
        style = self.specified_style()
        with PathPainting(gen, ctx, style, self.transform, self.clip) as paint:
            interpolate = get_enum(style, 'image-rendering', self.interpolations)
            filename  = self.get_filename()
            with TransformState(gen, ctx, self.img_translate @ self.img_scale, False):
                try:
                    if filename.endswith("jpeg") or filename.endswith("jpg"):
                        ctx.draw_image(gen.embed_jpg(filename, interpolate))
                    else:
                        ctx.draw_image(gen.load_image(filename, interpolate))
                except capypdf.CapyPDFException:
                    log(f"Can not load image: {self.get_filename()}")

class Text(PdfText, elements.TextElement):
    def on_page(self, page):
        """If any text element kern position appears on the page"""
        with page.contains(self) as checker:
            if checker.value is None:
                (xs, ys) = self.get_xy([], [])
                tr = self.composed_transform()
                p1 = tr.apply_to_point((min(xs), min(ys)))
                p2 = tr.apply_to_point((max(xs), max(ys)))
                checker.value = page.touches(BoundingBox((p1.x, p2.x), (p1.y, p2.y)))
            return checker.value

class Tspan(PdfText, elements.Tspan):
    def on_page(self, page):
        raise AttributeError("Do not call on_page on a sinly tspan, use the parent text node instead.")

class Pdf(inkex.OutputExtension):
    def add_arguments(self, pars):
        # NOTE: We need to take the pdf-version and if it's an X format, we can apply the bleed/margins. But otherwise we can not.
        pars.add_argument("--pdf-version", type=int, default=0)
        pars.add_argument("--bleed-marks", type=inkex.Boolean, default=False)
        pars.add_argument("--color-marks", type=inkex.Boolean, default=False)
        pars.add_argument("--intent", type=int, default=0)
        pars.add_argument("--overprint", type=inkex.Boolean, default=False, # TODO
            help="Converts any object with a multiply blend mode, into an overprint instead.")

    def save(self, stream):
        # Avoid re-saving the file since we may be saving a lot of data
        output = getattr(stream, 'name', None)
        if not output or not os.path.isfile(output):
            output = os.path.join(self.tempdir, "out.pdf")

        self.svg.tempdir = self.tempdir
        self.svg.to_pdf(output)

        # Save the result back if needed to the stream output
        if output != getattr(stream, 'name', None):
            with open(output, "rb") as fhl:
                stream.write(fhl.read())

# These are likely to be the maddest things to render
class Pattern(elements.Pattern):
    pass

class Gradient(elements.Gradient):
    pass

# Cope with inkex 1.3, remove in inkex 1.4
class Metadata(elements.Metadata):
    """Resource Description Framework (RDF) metadata"""
    title = property(lambda self: self._first_text("dc:title"))
    description = property(lambda self: self._first_text("dc:description"))

    rights = property(lambda self: self._first_text("dc:rights/cc:Agent/dc:title"))
    creator = property(lambda self: self._first_text("dc:creator/cc:Agent/dc:title"))
    publisher = property(lambda self: self._first_text("dc:publisher/cc:Agent/dc:title"))
    contributor = property(lambda self: self._first_text("dc:contributor/cc:Agent/dc:title"))

    date = property(lambda self: self._first_text("dc:date"))
    source = property(lambda self: self._first_text("dc:source"))
    language = property(lambda self: self._first_text("dc:language"))
    relation = property(lambda self: self._first_text("dc:relation"))
    coverage = property(lambda self: self._first_text("dc:coverage"))
    identifier = property(lambda self: self._first_text("dc:identifier"))

    def _first_text(self, loc):
        """Get the work title"""
        elem = self.findone(f"rdf:RDF/cc:Work/{loc}")
        if elem:
            return elem.text
        return None

    @property
    def tags(self):
        return [elem.text
            for elem in self.findall("rdf:RDF/cc:Work/dc:subject/rdf:Bag/rdf:li")]


class ColorProfile(HrefData, elements.BaseElement):
    tag_name = "color-profile"
    extensions = {None: 'icc'}

    @property
    def name(self):
        return self.get("name") or self.get_id()


class Color(colors.Color):
    """Support for icc profiles"""
    def __bool__(self):
        return self.iscolor(self, False)

    __nonzero__ = __bool__

    def parse_str(self, color):
        self.icc_name = None
        self.icc_values = []

        # Parse out the icc color information by itself
        if 'icc-color(' in color:
            values = color.lower().strip().strip(")").split("(")
            name, *values = values[0].split(",")
            self.icc_values = [float(v.strip()) for val in values]
            self.icc_name = name.strip()

        return super().parse_str(color)

    def for_pdf(self):
        if not self or len(self) < 3:
            return None

        color = capypdf.Color()
        # XXX Check ICC name with DeviceCMYK profile here
        if self.icc_name and len(self.icc_values) == 4:
            color.set_cmyk(*self.icc_values)
        elif len(self.to_rgb()) == 3:
            color.set_rgb(*self.to_rgb().to_floats())
        else:
            sys.stderr.write(f"not sure about color: {self}\n")
        return color

class TransformState(capypdf.StateContextManager):
    def __init__(self, gen, ctx, transform, clip):
        self.gen = gen
        self.clip = clip
        self.transform = transform
        # Clipping doesn't want to create state groups as this
        # would unapply some types of clipping commands.
        if clip is True:
            self.ctx = ctx
        else:
            super().__init__(ctx)

    def __enter__(self):
        super().__enter__()
        if self.transform:
            self.ctx.cmd_cm(*self.transform.to_hexad())
        if isinstance(self.clip, elements.ClipPath):
            self.clip.render_to_clip(self.gen, self.ctx)
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.clip is True:
            if self.transform:
                self.ctx.cmd_cm(*self.transform.inverse().to_hexad())
        else:
            return super().__exit__(exc_type, exc_value, exc_tb)


class StyleState(TransformState):
    LINECAPS = (
        ("butt", capypdf.LineCapStyle.Butt),
        ("round", capypdf.LineCapStyle.Round),
        ("square", capypdf.LineCapStyle.Projection_Square),
    )
    LINEJOINS = (
        ("miter", capypdf.LineJoinStyle.Miter),
        ("round", capypdf.LineJoinStyle.Round),
        ("bevel", capypdf.LineJoinStyle.Bevel),
    )
    BLENDMODES = (
        ("normal", capypdf.BlendMode.Normal),
        ("darken", capypdf.BlendMode.Darken),
        ("soft-light", capypdf.BlendMode.Softlight),
        ("hard-light", capypdf.BlendMode.Hardlight),
        ("difference", capypdf.BlendMode.Difference),
        ("exclusion", capypdf.BlendMode.Exclusion),
        ("hue", capypdf.BlendMode.Hue),
        ("color", capypdf.BlendMode.Color),
        ("saturation", capypdf.BlendMode.Saturation),
        ("overlay", capypdf.BlendMode.Overlay),
        ("multiply", capypdf.BlendMode.Multiply),
        ("color-burn", capypdf.BlendMode.Colorburn),
        ("color-dodge", capypdf.BlendMode.Colordodge),
        ("lighten", capypdf.BlendMode.Lighten),
        ("screen", capypdf.BlendMode.Screen),
    )

    def __init__(self, gen, ctx, style, transform, clip):
        self.style = style
        self.record = []
        self.fill_first = False
        self.fill = None
        self.stroke = None
        super().__init__(gen, ctx, transform, clip)

    def __getattr__(self, name):
        """Record all drawing commands for replay"""
        def _inner(*args):
            self.record.append((name, args))
        return _inner

    def get_float(self, name, default=None):
        return float(self.style.get(name, default))

    def get_enum(self, name, enum):
        return get_enum(self.style, name, enum)

    def __enter__(self):
        super().__enter__()
        try:
            self.fill = Color(self.style.get("fill", "none")).for_pdf()
        except colors.ColorIdError:
            self.fill = Color("red").for_pdf()
            sys.stderr.write("Gradient or Pattern ignored in fill\n")

        try:
            self.stroke = Color(self.style.get("stroke", "none")).for_pdf()
        except colors.ColorIdError:
            self.stroke = Color("red").for_pdf()
            sys.stderr.write("Gradient or Pattern ignored in stroke\n")

        # For now we're ignoring markers, assuming they aren't supported
        po = self.style.get("paint-order", "fill stroke markers")
        try:
            self.fill_first = po.index("fill") < po.index("stroke")
        except ValueError:
            self.fill_first = True

        return self

    def set_style(self, ctx):
        if self.clip is True:
            # When drawing a clipping region, there is no style
            return

        if self.fill:
            ctx.set_nonstroke(self.fill)

        if self.stroke:
            ctx.set_stroke(self.stroke)

        # TODO RE-Enable when Text supports these options in CapyPDF
        if self.stroke and hasattr(ctx, 'cmd_w'):
            ctx.cmd_w(self.get_float("stroke-width", 1))
            ctx.cmd_M(self.get_float("stroke-miterlimit", 10))
            ctx.cmd_J(self.get_enum("stroke-linecap", self.LINECAPS))
            ctx.cmd_j(self.get_enum("stroke-linejoin", self.LINEJOINS))

            if "stroke-dasharray" in self.style:
                try:
                    dash = [float(i) for i in self.style["stroke-dasharray"].replace(",", " ").split() if i]
                except ValueError:
                    dash = []
                if dash:
                    self.ctx.cmd_d(dash, float(self.style.get("stroke-dashoffset", 0)))

        #opacity = float(self.style.get("opacity", 1.0))

        # Not available on text nodes?
        if not hasattr(ctx, 'cmd_gs'):
            return

        # Specific opacity of each drawn element
        fill_opacity = minmax(self.style.get("fill-opacity", 1.0))
        stroke_opacity = minmax(self.style.get("stroke-opacity", 1.0))
        blendmode = self.get_enum("mix-blend-mode", self.BLENDMODES)
        if fill_opacity < 1.0 or stroke_opacity < 1.0 or blendmode != capypdf.BlendMode.Normal:
            gstate = capypdf.GraphicsState()
            gstate.set_ca(fill_opacity)
            gstate.set_CA(stroke_opacity)
            gstate.set_BM(blendmode)
            gsid = self.gen.add_graphics_state(gstate)
            ctx.cmd_gs(gsid)

    def replay_drawing(self, *args, **kwargs):
        """Replay all the drawing commands so we can duplicate drawing
           components for fill and stroke when stroke is behind fill.
        """
        raise NotImplementedError("Must provide replay_drawing")


class PathPainting(StyleState):
    """Applies to style to the context and forgets it when exiting"""
    def __init__(self, gen, ctx, style, transform, clip):
        self.closed = False
        self.evenodd = False
        super().__init__(gen, ctx, style, transform, clip)

    def __enter__(self):
        super().__enter__()
        self.evenodd = self.style.get("fill-rule", "nonzero") == "evenodd"
        self.set_style(self.ctx)
        # XXX Error out on markers here
        return self

    def replay_drawing(self, cmd):
        for name, args in self.record:
            getattr(self.ctx, name)(*args)
        getattr(self.ctx, f"cmd_{cmd}")()

    def close(self):
        self.closed = True

    def __exit__(self, exc_type, exc_value, exc_tb):
        star = ["", "star"][self.evenodd]
        if self.clip is True:
            # TODO: Find out if even odd, Wstar is needed here.
            self.replay_drawing("W")
        elif self.fill and self.stroke and self.fill_first:
            self.replay_drawing(["B", "b"][self.closed] + star)
        else:
            # Support for either-or and non-pdf paint-orders
            if self.fill and self.fill_first:
                self.replay_drawing("f" + star)
            if self.stroke:
                self.replay_drawing(["S", "s"][self.closed])
            if self.fill and not self.fill_first:
                self.replay_drawing("f" + star)
        return super().__exit__(exc_type, exc_value, exc_tb)


class TextPainting(StyleState):
    """Applies to style to the context and forgets it when exiting"""
    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.clip is True:
            self.replay_drawing(capypdf.TextMode.FillClip)
        elif self.fill and self.stroke and self.fill_first:
            self.replay_drawing(capypdf.TextMode.FillStroke)
        else:
            if self.fill and self.fill_first:
                self.replay_drawing(capypdf.TextMode.Fill)
            if self.stroke:
                self.replay_drawing(capypdf.TextMode.Stroke)
            if self.fill and not self.fill_first:
                self.replay_drawing(capypdf.TextMode.Fill)
        return super().__exit__(exc_type, exc_value, exc_tb)

    def replay_drawing(self, mode):
        text_ctx = self.ctx.text_new()
        text_ctx.cmd_Tr(mode)
        self.set_style(text_ctx)
        for name, args in self.record:
            getattr(text_ctx, name)(*args)
        self.ctx.render_text_obj(text_ctx)


if __name__ == "__main__":
    Pdf().run()
