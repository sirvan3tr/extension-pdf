#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2023 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
"""
Utilities for converting styles
"""

import re
import os

try:
    import fontconfig as fc
    from fontconfig import FC
except ImportError:
    raise ImportError("Python FontConfig is needed for font lookups")

# CSS to fontconfig lookup from ScientificInkscape
# https://github.com/burghoff/Scientific-Inkscape/
FC_WEIGHT = {
    'thin': FC.WEIGHT_THIN,
    'ultralight': FC.WEIGHT_EXTRALIGHT, 
    'light': FC.WEIGHT_LIGHT, 
    'semilight': FC.WEIGHT_SEMILIGHT, 
    'book': FC.WEIGHT_BOOK, 
    'normal': FC.WEIGHT_NORMAL, 
    'medium': FC.WEIGHT_MEDIUM, 
    'semibold': FC.WEIGHT_SEMIBOLD, 
    'bold': FC.WEIGHT_BOLD, 
    'ultrabold': FC.WEIGHT_ULTRABOLD, 
    'heavy': FC.WEIGHT_HEAVY, 
    'ultraheavy': FC.WEIGHT_ULTRABLACK, 
    '100': FC.WEIGHT_THIN, 
    '200': FC.WEIGHT_EXTRALIGHT, 
    '300': FC.WEIGHT_LIGHT, 
    '350': FC.WEIGHT_SEMILIGHT, 
    '380': FC.WEIGHT_BOOK, 
    '400': FC.WEIGHT_NORMAL, 
    '500': FC.WEIGHT_MEDIUM, 
    '600': FC.WEIGHT_SEMIBOLD, 
    '700': FC.WEIGHT_BOLD, 
    '800': FC.WEIGHT_ULTRABOLD, 
    '900': FC.WEIGHT_HEAVY, 
    '1000': FC.WEIGHT_ULTRABLACK,
}

FC_SLANT = {
    'normal': FC.SLANT_ROMAN, 
    'italic': FC.SLANT_ITALIC, 
    'oblique': FC.SLANT_OBLIQUE,
}

FC_STRETCH = {
    'ultra-condensed': FC.WIDTH_ULTRACONDENSED, 
    'extra-condensed': FC.WIDTH_EXTRACONDENSED, 
    'condensed': FC.WIDTH_CONDENSED, 
    'semi-condensed': FC.WIDTH_SEMICONDENSED, 
    'normal': FC.WIDTH_NORMAL, 
    'semi-expanded': FC.WIDTH_SEMIEXPANDED, 
    'expanded': FC.WIDTH_EXPANDED, 
    'extra-expanded': FC.WIDTH_EXTRAEXPANDED, 
    'ultra-expanded': FC.WIDTH_ULTRAEXPANDED,
}

def get_font_filename(style):
    """Use fontconfig to attempt to get the filename to the ttf from the inkex.Style object"""
    # TODO: Add Inkscape custom directory locations here
    fc_config = fc.Config.get_current()

    family_name = style.get('font-family', "sans").replace("'", '').replace('"', '')
    fc_pattern = fc.Pattern.name_parse(re.escape(family_name))

    fc_pattern.add(fc.PROP.WIDTH, FC_STRETCH.get(style.get('font-stretch'), FC.WIDTH_NORMAL))
    fc_pattern.add(fc.PROP.WEIGHT, FC_WEIGHT.get(style.get('font-weight'), FC.WEIGHT_NORMAL))
    fc_pattern.add(fc.PROP.SLANT, FC_SLANT.get(style.get('font-style'), FC.SLANT_ROMAN))

    fc_config.substitute(fc_pattern, fc.FC.MatchPattern)
    fc_pattern.default_substitute()

    # Note for future, some fonts don't have some chars and we might have to look them up
    #fontcharsets[tuple(truefont.items())] = found.get(fc.PROP.CHARSET,0)[0]

    found, status = fc_config.font_match(fc_pattern)
    fn = found.get(fc.PROP.FILE, 0)[0]
    if os.path.isfile(fn):
        return fn

    return None

