# PDF Output

This extension takes an SVG from Inkscape and outputs a PDF/x3 pdf file.

This will follow the functions available in the libcapypdf library and will depend on pre-processing steps in inkscape's extensions system to turn the svg from inkscape-svg to a something simpler which can be rendered into PDF without too many issues.

If you pass an SVG file without these steps you may find reduced functionality.

# Installing

To use this extension you will need to install the CapyPDF python binary wheel. Wheels are available for windows and linux at this time.


