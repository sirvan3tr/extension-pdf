# coding=utf-8
#
# Copyright (C) 2023 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
"""
Record all capypdf commands for test output
"""

import capypdf
from capypdf import *

from collections import defaultdict

TEMPLATE = """#coding=utf-8
# Autogenerated by inkscape-pdfout-recorder, DO NOT EDIT

import sys
import capypdf

def main(output):
{CODE}

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.stderr.write("Please specify an output filename!\\n")
        sys.exit(2)
    main(output=sys.argv[1])
"""

class RecordedClass:
    # Clear by using RecordedClass.record = []
    stack = []
    record = []
    counts = defaultdict(int)
    _varname = None
    
    def __init__(self, *args, obj=None, attr=None):
        self.varname = f"{self._varname}{self.counts[self._varname]}"
        self.counts[self._varname] += 1
        if obj is not None:
            RecordedClass.record.append((self, obj, attr, args))
        else:
            RecordedClass.record.append((self, args))

    def __getattr__(self, name):
        def _inner(*args):
            RecordedClass.record.append((None, self, name, args))
        return _inner

    def __enter__(self):
        new_record = []
        RecordedClass.record.append(('with', RecordedClass.record.pop(), new_record))
        RecordedClass.stack.append(RecordedClass.record)
        RecordedClass.record = new_record
        return self

    def __exit__(self, *args, **kwargs):
        RecordedClass.record = RecordedClass.stack.pop()

    def output_record(self):
        yield from self._output_records(RecordedClass.record)
        RecordedClass.record = []

    def _output_records(self, lst, tab=1):
        for row in lst:
            yield from self._output_record(row, tab)

    def _output_record(self, row, tab=1):
        tout = " " * tab * 4
        if len(row) == 2:
            obj, args = row
            yield f"{tout}{obj.varname} = capypdf.{obj.get_record_name()}({self._format_args(args)})"
        elif len(row) == 4:
            ret, obj, attr, args = row
            reteq = f"{ret.varname} = " if ret is not None else ""
            yield f"{tout}{reteq}{obj.varname}.{attr}({self._format_args(args)})"
        elif len(row) == 3:
            cmd, ref, sublst = row
            if cmd == 'with':
                line = next(self._output_record(ref, 0)).split(' = ')
                if len(line) == 2:
                    yield f"{tout}with {line[1]} as {line[0]}:"
                else:
                    yield f"{tout}with {line[0]}:"
            yield from self._output_records(sublst, tab+1)

    def get_record_name(self):
        return type(self).__name__

    def _format_args(self, args):
        return ", ".join([self._format_arg(a) for a in args])

    def _format_arg(self, arg):
        if isinstance(arg, (RecordedClass, StaticVar)):
            return arg.varname
        elif isinstance(arg, str):
            return f'"{arg}"'
        elif isinstance(arg, (int, float)):
            return f"{arg:.3g}"
        elif isinstance(arg, list):
            return f"[{self._format_args(arg)}]"
        elif isinstance(arg, tuple):
            return f"({self._format_args(arg)})"
        elif isinstance(arg, capypdf.Enum):
            return f"capypdf.{type(arg).__name__}.{arg._name_}"
        raise ValueError(f"Can't parse arg type {arg}\n")

class StaticVar:
    def __init__(self, varname):
        self.varname = varname

class Options(RecordedClass):
    _varname = 'opt'

class PageProperties(RecordedClass):
    _varname = 'prop'

class Generator(RecordedClass):
    _varname = 'gen'

    def __init__(self, output, options):
        super().__init__(StaticVar('output'), options)
        self._out = output
        self._opt = options

    def __exit__(self, *args, **kwargs):
        super().__exit__(*args, **kwargs)
        with open(self._out, 'w') as fhl:
            fhl.write(TEMPLATE.format(CODE="\n".join(self.output_record())))

    def page_draw_context(self):
        return DrawContext(obj=self, attr='page_draw_context')

    def text_width(self, text, font, size):
        return self._gen.text_width(text, font._font, size)

    def load_font(self, *args):
        font = Font(*args, obj=self, attr='load_font')
        # Keep a real copy for font width calculations
        with capypdf.Generator("/tmp/nothing.pdf", self._opt) as gen:
            font._font = gen.load_font(*args)
        return font

class StateContextManager(RecordedClass):
    _varname = 'state'

    def __init__(self, *args, obj=None, attr=None):
        self.ctx = args[0]
        super().__init__(*args, obj=obj, attr=attr)

    def get_record_name(self):
        return 'StateContextManager'

class Font(RecordedClass):
    _varname = 'font'

class DrawContext(RecordedClass):
    _varname = 'ctx'

class Color(RecordedClass):
    _varname = 'color'

class GraphicsState(RecordedClass):
    _varname = 'gs'

